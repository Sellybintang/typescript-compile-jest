export var Role;
(function (Role) {
    Role[Role["User"] = 0] = "User";
    Role[Role["Admin"] = 1] = "Admin";
    Role[Role["SuperAdmin"] = 2] = "SuperAdmin";
})(Role || (Role = {}));
export var Roles;
(function (Roles) {
    Roles["User"] = "user";
    Roles["Admin"] = "admin";
    Roles["SuperAdmin"] = "superadmin";
})(Roles || (Roles = {}));
